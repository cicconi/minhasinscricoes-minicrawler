var cheerio = require('cheerio');
var http = require('http');

var options = {
  host: "www.minhasinscricoes.com.br",
  path: "/TABELA-LATERAL-NOVA/2012/TABELACOMPLETA-DATA.aspx",
};


var request = http.request(options, function(res){
  var data = '';

  res.on('data', function(chunk){
    data += chunk;
  });

  res.on('end', function () {
    $ = cheerio.load(data, {
      normalizeWhitespace: true,
      xmlMode: true,
      decodeEntities: false
    });
    var eventos = $('.lista-clientes tr');
    var parsedEventos = [];

    eventos.each(function(i, tr){
      parsedEventos.push({ 
        nome: $('td', tr).eq(0).text(),
        localizacao: $('td', tr).eq(1).text(),
        data: $('td', tr).eq(2).text()
      });
    });

    console.log(JSON.stringify(parsedEventos));

  });
});

request.on('error', function (e) {
  console.log(e.message)
});

request.end();